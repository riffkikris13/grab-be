CREATE TABLE customers (
    id SERIAL PRIMARY KEY,
    customer_name VARCHAR(100) NOT NULL
);

CREATE TABLE orders (
    id VARCHAR(200) PRIMARY KEY,
    state_id INT NOT NULL,
    customer_id INT NOT NULL,
    address VARCHAR(150) NOT NULL,
    created_date TIMESTAMP,
    modified_date TIMESTAMP,
    FOREIGN KEY (state_id) REFERENCES order_states(id),
    FOREIGN KEY (customer_id) REFERENCES customers(id)
);

CREATE TABLE order_states (
    id SERIAL PRIMARY KEY,
    state_name VARCHAR(20) NOT NULL,
    created_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    modified_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

db.createCollection("orders", {
    validator: {
        $jsonSchema: {
            bsonType: "object",
            required: ["state", "customer", "address", "created_date"],
            properties: {
                state: {
                    bsonType: "object",
                    required: ["id", "state_name"],
                    properties: {
                        id: {
                            bsonType: "long",
                            description: "must be an long and is required"
                        },
                        state_name: {
                            bsonType: "string",
                            description: "must be a string and is required"
                        }
                    },
                    description: "must be an object and is required"
                },
                customer: {
                    bsonType: "object",
                    required: ["id", "customer_name"],
                    properties: {
                        id: {
                            bsonType: "long",
                            description: "must be an long and is required"
                        },
                        customer_name: {
                            bsonType: "string",
                            description: "must be a string and is required"
                        }
                    },
                    description: "must be an object and is required"
                },
                address: {
                    bsonType: "string",
                    description: "must be a string and is required"
                },
                created_date: {
                    bsonType: "date",
                    description: "must be a date and is required"
                },
                modified_date: {
                    bsonType: "date",
                    description: "must be a date and is required"
                }
            }
        }
    }
});

db.orders.createIndex( { "created_date": 1 }, { expireAfterSeconds: 604800 } )