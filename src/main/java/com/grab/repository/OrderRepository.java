package com.grab.repository;

import com.grab.model.mongo.Order;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends MongoRepository<Order, ObjectId> {
    Page<Order> findByCustomerIdAndStateId(Long customerId, Long stateId, Pageable pageable);
}
