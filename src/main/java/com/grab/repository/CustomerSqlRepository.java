package com.grab.repository;

import com.grab.model.postgre.CustomerSql;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerSqlRepository extends JpaRepository<CustomerSql, Long> {
    Page<CustomerSql> findByCustomerNameStartingWithIgnoreCase(String customerName, Pageable pageable);
}
