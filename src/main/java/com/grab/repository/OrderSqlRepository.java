package com.grab.repository;

import com.grab.model.mongo.Order;
import com.grab.model.postgre.OrderSql;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;

@Repository
public interface OrderSqlRepository extends JpaRepository<OrderSql, String>, JpaSpecificationExecutor<OrderSql>, PagingAndSortingRepository<OrderSql, String> {
    Page<OrderSql> findByCustomerSqlIdAndStateId(Long customerId, Long stateId, Pageable pageable);

    Long countByState_Id(Long stateId);
}
