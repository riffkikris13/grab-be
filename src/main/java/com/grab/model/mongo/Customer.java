package com.grab.model.mongo;

import jakarta.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
@Builder
public class Customer {
    @NotNull
    @Field("id")
    private Long id;

    @NotNull
    @Field("customer_name")
    private String customerName;
}
