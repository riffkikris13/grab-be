package com.grab.model.mongo;

import jakarta.persistence.PostLoad;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

@Document(collection = "orders")
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Order {

    @Id
    @Field("_id")
    private ObjectId id;

    @Field("state")
    private State state;

    @Field("customer")
    private Customer customer;

    @Field("address")
    private String address;

    @Field("created_date")
    private LocalDateTime createdDate;

    @Field("modified_date")
    private LocalDateTime modifiedDate;

    @PostLoad
    public void convertToLocalDateTime() {
        if (createdDate != null) {
            ZonedDateTime zonedDateTime = createdDate.atZone(ZoneId.of("UTC"));
            this.createdDate = zonedDateTime.withZoneSameInstant(ZoneId.systemDefault()).toLocalDateTime();
        }
        if (modifiedDate != null) {
            ZonedDateTime zonedDateTime = modifiedDate.atZone(ZoneId.of("UTC"));
            this.modifiedDate = zonedDateTime.withZoneSameInstant(ZoneId.systemDefault()).toLocalDateTime();
        }
    }
}