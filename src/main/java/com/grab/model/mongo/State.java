package com.grab.model.mongo;

import jakarta.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
@Builder
public class State {
    @NotNull
    @Field("id")
    private Long id;

    @NotNull
    @Field("state_name")
    private String stateName;
}
