package com.grab.model.postgre;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

@Entity
@Table(name = "order_states")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderState {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "state_name", nullable = false, length = 20)
    private String stateName;

    @Column(name = "created_date", nullable = false, updatable = false)
    private LocalDateTime createdDate;

    @Column(name = "modified_date")
    private LocalDateTime modifiedDate;

    @PostLoad
    public void convertToLocalDateTime() {
        if (createdDate != null) {
            ZonedDateTime zonedDateTime = createdDate.atZone(ZoneId.of("UTC"));
            this.createdDate = zonedDateTime.withZoneSameInstant(ZoneId.systemDefault()).toLocalDateTime();
        }
        if (modifiedDate != null) {
            ZonedDateTime zonedDateTime = modifiedDate.atZone(ZoneId.of("UTC"));
            this.modifiedDate = zonedDateTime.withZoneSameInstant(ZoneId.systemDefault()).toLocalDateTime();
        }
    }
}
