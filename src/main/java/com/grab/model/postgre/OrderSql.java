package com.grab.model.postgre;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

@Entity
@Table(name = "orders")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderSql {
    @Id
    @Column(name = "id", length = 200)
    private String id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "state_id", nullable = false)
    private OrderState state;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "customer_id", nullable = false)
    private CustomerSql customerSql;

    @Column(name = "address", nullable = false, length = 150)
    private String address;

    @Column(name = "created_date", nullable = false, updatable = false)
    private LocalDateTime createdDate;

    @Column(name = "modified_date")
    private LocalDateTime modifiedDate;

    @PostLoad
    public void convertToLocalDateTime() {
        if (createdDate != null) {
            ZonedDateTime zonedDateTime = createdDate.atZone(ZoneId.of("UTC"));
            this.createdDate = zonedDateTime.withZoneSameInstant(ZoneId.systemDefault()).toLocalDateTime();
        }
        if (modifiedDate != null) {
            ZonedDateTime zonedDateTime = modifiedDate.atZone(ZoneId.of("UTC"));
            this.modifiedDate = zonedDateTime.withZoneSameInstant(ZoneId.systemDefault()).toLocalDateTime();
        }
    }
}
