package com.grab.exception;

public class DataIsNotExistException extends Exception {
    public DataIsNotExistException(String message) {
        super(message);
    }
}
