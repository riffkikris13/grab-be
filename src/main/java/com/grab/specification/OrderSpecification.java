package com.grab.specification;

import com.grab.model.postgre.OrderSql;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.jpa.domain.Specification;

import java.time.LocalDateTime;

@Slf4j
public class OrderSpecification {

    public static Specification<OrderSql> hasStateId(Long stateId) {
        return (root, query, builder) -> stateId == null ? builder.conjunction() : builder.equal(root.get("state").get("id"), stateId);
    }

    public static Specification<OrderSql> hasCustomerId(Long customerId) {
        return (root, query, builder) -> customerId == null ? builder.conjunction() : builder.equal(root.get("customerSql").get("id"), customerId);
    }

    public static Specification<OrderSql> createdDateBetween(LocalDateTime startDate, LocalDateTime endDate) {
        return (root, query, builder) -> {
            if (startDate != null && endDate != null) {
                return builder.between(root.get("createdDate"), startDate, endDate);
            } else if (startDate != null) {
                return builder.greaterThanOrEqualTo(root.get("createdDate"), startDate);
            } else if (endDate != null) {
                return builder.lessThanOrEqualTo(root.get("createdDate"), endDate);
            } else {
                return builder.conjunction();
            }
        };
    }
}