package com.grab.dto;

import com.grab.model.mongo.Customer;
import com.grab.model.mongo.State;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class OrderReq {
    private String id;

    @NotNull
    @Valid
    private State state;

    @NotNull
    private Customer customer;

    @NotNull
    private String address;
}
