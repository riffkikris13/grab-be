package com.grab.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class OrderStatisticRes {
    private Long totalOrders;

    private Long totalOngoingOrders;

    private Long totalCompletedOrders;
}
