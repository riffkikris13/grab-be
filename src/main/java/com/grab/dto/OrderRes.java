package com.grab.dto;

import com.grab.model.mongo.Customer;
import com.grab.model.mongo.State;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class OrderRes {
    private String id;

    private State state;

    private Customer customer;

    private String address;

    private LocalDateTime createdDate;
}
