package com.grab.controller;

import com.grab.dto.OrderPagingRes;
import com.grab.dto.OrderReq;
import com.grab.dto.OrderRes;
import com.grab.dto.OrderStatisticRes;
import com.grab.exception.DataIsNotExistException;
import com.grab.model.postgre.CustomerSql;
import com.grab.model.postgre.OrderState;
import com.grab.service.OrderService;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/order")
@AllArgsConstructor
public class OrderController {
    private OrderService orderService;

    @PostMapping
    private ResponseEntity<String> createOrder(@Valid @RequestBody OrderReq orderReq) {
        orderService.createOrder(orderReq);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PatchMapping
    private ResponseEntity<String> updateOrder(@Valid @RequestBody OrderReq orderReq) throws DataIsNotExistException {
        orderService.updateOrder(orderReq);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping("/{id}")
    private ResponseEntity<OrderRes> getOrderById(@PathVariable String id) throws DataIsNotExistException {
        return new ResponseEntity<>(orderService.getOrderById(id), HttpStatus.OK);
    }

    @GetMapping("/ongoing/{id}")
    private ResponseEntity<OrderPagingRes> getOngoingOrdersByCustomerId(@PathVariable Long id, @RequestParam int page, @RequestParam int size) {
        return new ResponseEntity<>(orderService.getOngoingOrdersByCustomerId(id, page, size), HttpStatus.OK);
    }

    @GetMapping
    private ResponseEntity<OrderPagingRes> getOrderByManyConditions(@RequestParam(required = false) Long stateId, @RequestParam(required = false) Long customerId, @RequestParam(required = false) String startDate, @RequestParam(required = false) String endDate, @RequestParam int page, @RequestParam int size) {
        return new ResponseEntity<>(orderService.getOrderByManyConditions(stateId, customerId, startDate, endDate, page, size), HttpStatus.OK);
    }

    @GetMapping("/statistics")
    private ResponseEntity<OrderStatisticRes> getOrderStatistics() {
        return new ResponseEntity<>(orderService.getOrderStatistics(), HttpStatus.OK);
    }

    @GetMapping("/order-states")
    private ResponseEntity<List<OrderState>> getOrderStates() {
        return new ResponseEntity<>(orderService.getOrderStates(), HttpStatus.OK);
    }

    @GetMapping("/customers/{name}")
    private ResponseEntity<List<CustomerSql>> getOrderStates(@PathVariable("name") String name) {
        return new ResponseEntity<>(orderService.getCustomersByName(name), HttpStatus.OK);
    }
}
