package com.grab.util;

import com.grab.dto.OrderReq;
import com.grab.dto.OrderRes;
import com.grab.model.mongo.Customer;
import com.grab.model.mongo.Order;
import com.grab.model.mongo.State;
import com.grab.model.postgre.OrderSql;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericRecord;
import org.bson.types.ObjectId;

import java.time.ZoneId;
import java.util.List;
import java.util.stream.Collectors;

public class Converter {
    private static Schema schema = new Schema.Parser().parse("{\"type\":\"record\",\"name\":\"Order\",\"namespace\":\"com.order\",\"fields\":[{\"name\":\"id\",\"type\":\"string\"},{\"name\":\"stateId\",\"type\":\"int\"},{\"name\":\"customerId\",\"type\":\"int\"},{\"name\":\"address\",\"type\":\"string\"},{\"name\":\"createdDate\",\"type\":{\"type\":\"long\",\"logicalType\":\"timestamp-millis\"}},{\"name\":\"modifiedDate\",\"type\":[\"null\",{\"type\":\"long\",\"logicalType\":\"timestamp-millis\"}],\"default\":null}]}");

    public static Order reqToOrderConverter(OrderReq req) {
        return Order.builder()
                .id(req.getId() == null ? null : new ObjectId(req.getId()))
                .state(req.getState())
                .customer(req.getCustomer())
                .address(req.getAddress())
                .build();
    }

    public static OrderRes orderToOrderRes(Order order) {
        return OrderRes.builder()
                .id(order.getId().toString())
                .state(order.getState())
                .customer(order.getCustomer())
                .address(order.getAddress())
                .createdDate(order.getCreatedDate())
                .build();
    }

    public static List<OrderRes> ordersToOrderRes(List<Order> orders) {
        return orders.stream().map(data -> OrderRes.builder()
                .id(data.getId().toString())
                .state(data.getState())
                .customer(data.getCustomer())
                .address(data.getAddress())
                .createdDate(data.getCreatedDate())
                .build()).collect(Collectors.toList());
    }

    public static OrderRes orderSqlToOrderRes(OrderSql order) {
        return OrderRes.builder()
                .id(order.getId())
                .state(State.builder().id(order.getState().getId()).stateName(order.getState().getStateName()).build())
                .customer(Customer.builder().id(order.getCustomerSql().getId()).customerName(order.getCustomerSql().getCustomerName()).build())
                .address(order.getAddress())
                .createdDate(order.getCreatedDate())
                .build();
    }

    public static List<OrderRes> orderSqlsToOrderRes(List<OrderSql> orders) {
        return orders.stream().map(data -> OrderRes.builder()
                .id(data.getId())
                .state(State.builder().id(data.getState().getId()).stateName(data.getState().getStateName()).build())
                .customer(Customer.builder().id(data.getCustomerSql().getId()).customerName(data.getCustomerSql().getCustomerName()).build())
                .address(data.getAddress())
                .createdDate(data.getCreatedDate())
                .build()).collect(Collectors.toList());
    }

    public static GenericRecord orderToKafkaMessage(Order order) {
        GenericRecord orderData = new GenericData.Record(schema);
        orderData.put("id", order.getId().toString());
        orderData.put("stateId", order.getState().getId());
        orderData.put("customerId", order.getCustomer().getId());
        orderData.put("address", order.getAddress());
        orderData.put("createdDate", order.getCreatedDate().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());
        orderData.put("modifiedDate", order.getModifiedDate() == null ? null : order.getModifiedDate().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());

        return orderData;
    }

    public static GenericRecord orderSqlToKafkaMessage(OrderSql order) {
        GenericRecord orderData = new GenericData.Record(schema);
        orderData.put("id", order.getId());
        orderData.put("stateId", order.getState().getId());
        orderData.put("customerId", order.getCustomerSql().getId());
        orderData.put("address", order.getAddress());
        orderData.put("createdDate", order.getCreatedDate().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());
        orderData.put("modifiedDate", order.getModifiedDate() == null ? null : order.getModifiedDate().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());

        return orderData;
    }
}
