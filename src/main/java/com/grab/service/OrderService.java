package com.grab.service;

import com.grab.dto.*;
import com.grab.exception.DataIsNotExistException;
import com.grab.model.postgre.CustomerSql;
import com.grab.model.mongo.Order;
import com.grab.model.postgre.OrderSql;
import com.grab.model.postgre.OrderState;
import com.grab.repository.CustomerSqlRepository;
import com.grab.repository.OrderRepository;
import com.grab.repository.OrderSqlRepository;
import com.grab.repository.OrderStateRepository;
import com.grab.specification.OrderSpecification;
import com.grab.util.Converter;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.avro.generic.GenericRecord;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
@Slf4j
public class OrderService {
    private OrderRepository orderRepository;

    private OrderSqlRepository orderSqlRepository;

    private OrderStateRepository orderStateRepository;

    private CustomerSqlRepository customerSqlRepository;

    private KafkaTemplate<String, GenericRecord> kafkaTemplate;

    public void createOrder(OrderReq req) {
        Order order = Converter.reqToOrderConverter(req);
        order.setCreatedDate(LocalDateTime.now());

        Order savedOrder = orderRepository.save(order);
        if(savedOrder != null) {
            kafkaTemplate.send(new ProducerRecord<>("order", savedOrder.getId().toString(), Converter.orderToKafkaMessage(savedOrder)));
        }
    }

    public void updateOrder(OrderReq req) throws DataIsNotExistException {
        Optional<Order> existedOrder = orderRepository.findById(new ObjectId(req.getId()));
        if(existedOrder.isPresent()) {
            Order orderData = existedOrder.get();
            orderData.setState(req.getState());
            orderData.setCustomer(req.getCustomer());
            orderData.setAddress(req.getAddress());
            orderData.setModifiedDate(LocalDateTime.now());
            orderRepository.save(orderData);

            kafkaTemplate.send(new ProducerRecord<>("order", orderData.getId().toString(), Converter.orderToKafkaMessage(orderData)));
        } else {
            Optional<OrderSql> existedOrderSql = orderSqlRepository.findById(req.getId());
            if(existedOrderSql.isEmpty()) {
                throw new DataIsNotExistException("Order is not exist");
            }

            OrderSql orderData = existedOrderSql.get();
            orderData.setState(OrderState.builder().id(req.getState().getId()).build());
            orderData.setCustomerSql(CustomerSql.builder().id(req.getCustomer().getId()).build());
            orderData.setAddress(req.getAddress());
            orderData.setModifiedDate(LocalDateTime.now());

            kafkaTemplate.send(new ProducerRecord<>("order", orderData.getId(), Converter.orderSqlToKafkaMessage(orderData)));
        }
    }

    public OrderRes getOrderById(String id) throws DataIsNotExistException {
        Optional<Order> existedOrder = orderRepository.findById(new ObjectId(id));
        if(existedOrder.isPresent()) {
            return Converter.orderToOrderRes(existedOrder.get());
        } else {
            Optional<OrderSql> existedOrderSql = orderSqlRepository.findById(id);
            if(existedOrderSql.isEmpty()) {
                throw new DataIsNotExistException("Order is not exist");
            }

            return Converter.orderSqlToOrderRes(existedOrderSql.get());
        }
    }

    public OrderPagingRes getOngoingOrdersByCustomerId(Long id, int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Order> orderPage = orderRepository.findByCustomerIdAndStateId(id, 1L, pageable);

        if(orderPage.getTotalElements() == 0) {
            Page<OrderSql> orderSqlPage = orderSqlRepository.findByCustomerSqlIdAndStateId(id, 1L, pageable);

            return OrderPagingRes.builder()
                    .data(Converter.orderSqlsToOrderRes(orderSqlPage.getContent()))
                    .totalData(orderSqlPage.getTotalElements())
                    .totalPage(orderSqlPage.getTotalPages())
                    .build();
        }

        return OrderPagingRes.builder()
                .data(Converter.ordersToOrderRes(orderPage.getContent()))
                .totalData(orderPage.getTotalElements())
                .totalPage(orderPage.getTotalPages())
                .build();
    }

    public OrderPagingRes getOrderByManyConditions(Long stateId, Long customerId, String startDate, String endDate, int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<OrderSql> orderSqlPage = orderSqlRepository.findAll(Specification.where(OrderSpecification.hasStateId(stateId))
                .and(OrderSpecification.hasCustomerId(customerId))
                .and(OrderSpecification.createdDateBetween(startDate.isEmpty() ? null : LocalDateTime.parse(startDate), endDate.isEmpty() ? null : LocalDateTime.parse(endDate).plusDays(1))), pageable);

        return OrderPagingRes.builder()
                .data(Converter.orderSqlsToOrderRes(orderSqlPage.getContent()))
                .totalData(orderSqlPage.getTotalElements())
                .totalPage(orderSqlPage.getTotalPages())
                .build();
    }

    public OrderStatisticRes getOrderStatistics() {
        return OrderStatisticRes.builder()
                .totalOrders(orderSqlRepository.count())
                .totalOngoingOrders(orderSqlRepository.countByState_Id(1L))
                .totalCompletedOrders(orderSqlRepository.countByState_Id(2L))
                .build();
    }

    public List<OrderState> getOrderStates() {
        return orderStateRepository.findAll();
    }

    public List<CustomerSql> getCustomersByName(String name) {
        Pageable pageable = PageRequest.of(0, 20);
        Page<CustomerSql> customers = customerSqlRepository.findByCustomerNameStartingWithIgnoreCase(name, pageable);
        return customers.getContent();
    }
}
